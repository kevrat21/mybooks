package com.papb.mybooks_kotlin.utils

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.papb.mybooks_kotlin.databinding.EachBooksItemBinding

class BooksAdapter(private val list: MutableList<BooksData>) :
    RecyclerView.Adapter<BooksAdapter.BooksViewHolder>() {

    private var listener:BooksAdapterClickInterface? = null

    fun setListener(listener:BooksAdapterClickInterface){
        this.listener = listener
    }
    inner class BooksViewHolder(val binding:EachBooksItemBinding):RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BooksViewHolder {
        val binding = EachBooksItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BooksViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BooksViewHolder, position: Int) {
        with(holder){
            with(list[position]){
                binding.bookTitleItem.text = this.Buku

                binding.deleteTask.setOnClickListener(){
                    listener?.onDeleteBookBtnClicked(this)
                }

                binding.editBook.setOnClickListener(){
                    listener?.onEditBookBtnClicked(this)
                }
            }
        }
    }
    override fun getItemCount(): Int {
        return list.size
    }

    interface BooksAdapterClickInterface{
        fun onDeleteBookBtnClicked(booksData: BooksData)
        fun onEditBookBtnClicked(booksData: BooksData)
    }
}